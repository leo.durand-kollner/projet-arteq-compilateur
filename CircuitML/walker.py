from tatsu.walkers import NodeWalker # Explore AST

from expr import *

class CircuitMLWalker (NodeWalker):
    def __init__(self):
        NodeWalker.__init__(self)
        self.context = {} # Variables values
        self.used_varnames = set() # Used variable names

    def walk_object (self, node):
        print("Error : found node of type", type(node))
        return node

    def walk_str (self, node):
        print("Var", node)
        return CircuitMLVar (node, self)

    def walk__boolean (self, node):
        print(node.value)
        if node.value == "True":
            return CircuitMLBool (True, self)
        return CircuitMLBool (False, self)

    def walk__not (self, node):
        print("Not")
        return CircuitMLNot (node, self)

    def walk__and (self, node):
        print("And")
        return CircuitMLAnd (node, self)

    def walk__or (self, node):
        print("Or")
        return CircuitMLOr (node, self)

    def walk__nand (self, node):
        print("Nand")
        return CircuitMLNand (node, self)

    def walk__first(self, node):
        print("First")
        return CircuitMLFirst (node, self)

    def walk__second(self, node):
        print("Second")
        return CircuitMLSecond (node, self)

    def walk__pair(self, node):
        print("Pair")
        return CircuitMLPair (node, self)

    def walk__lambda(self, node):
        print("Lambda")
        return CircuitMLLambda (node, self)

    def walk__assignment(self, node):
        print("Assignment")
        return CircuitMLAssignment (node, self)

    def walk__composite(self, node):
        print("Composite")
        return CircuitMLComposite (node, self)

    def walk__condition(self, node):
        print("Condition")
        return CircuitMLCondition (node, self)