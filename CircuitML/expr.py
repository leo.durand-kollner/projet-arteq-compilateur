import tatsu

from abc import ABC, abstractmethod
from inspect import getmro as getancestors
from copy import copy

class CircuitMLExpr (ABC):
    """
    Class to represent a general CircuitML expression
    """
    def __init__ (self, node, walker):
        self.node = node
        self.walker = walker

    def substitute (self, debug=True):
        """
        Substitute all variables by available expressions (in [context])
        Should return an expression without modifying substitute's called expression
        """
        return self

    def get_used_varnames (self):
        """
        Get used variables (different from walker context since some variables can be undefined)
        """
        return set()


class CircuitMLBool (CircuitMLExpr):
    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.value = bool(node)


class CircuitMLVar (CircuitMLExpr):
    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.var = node

    def substitute (self, debug=True):
        if self.var in self.walker.context:
            return self.walker.context[self.var].substitute(debug=debug)
        return self

    def get_used_varnames (self):
        return set(self.var)


class CircuitMLPair (CircuitMLExpr):
    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.left = walker.walk(node.left)
        self.right = walker.walk(node.right)

    def substitute (self, debug=True):
        left = self.left.substitute(debug=debug)
        if not left:
            return None

        right = self.right.substitute(debug=debug)
        if not right:
            return None

        ret = copy(self)
        ret.left = left
        ret.right = right
        return ret

    def get_used_varnames (self):
        return self.left.get_used_varnames().union(self.right.get_used_varnames())


class CircuitMLNot (CircuitMLExpr):
    def operation (self, x):
        return CircuitMLBool (not x, self.walker)

    def get_used_varnames (self):
        return set()

# A general operation on two arguments : f : (x,y)
class CircuitMLOperation (CircuitMLExpr, ABC):
    @abstractmethod
    def operation (self, x, y):
        pass

    def get_used_varnames (self):
        return set()

class CircuitMLLogicalOperation (CircuitMLOperation, ABC):
    pass

class CircuitMLAnd (CircuitMLLogicalOperation):
    def operation (self, x, y):
        return CircuitMLBool (x and y, self.walker)

class CircuitMLOr (CircuitMLLogicalOperation):
    def operation (self, x, y):
        return CircuitMLBool (x or y, self.walker)

class CircuitMLNand (CircuitMLLogicalOperation):
    def operation (self, x, y):
        return CircuitMLBool (not (x and y), self.walker)

class CircuitMLFirst (CircuitMLOperation):
    def substitute (self, debug=True):
        return self

    def operation (self, x,y):
        return x

class CircuitMLSecond (CircuitMLOperation):
    def operation (self, x,y):
        return y


class CircuitMLLambda (CircuitMLExpr):

    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.arg = CircuitMLVar(node.arg, walker)
        self.res = walker.walk(node.res)

    def substitute (self, debug=True):
        # First check if argument variable name is already used
        # Example of where this is needed : """y = x; f = Lambda x : & (x, y); x = False; f True"""
        # This should return False, not True
        ret = copy(self)
        while ret.arg.var in self.walker.used_varnames:
            # Rename arg variable to [varname] + "x"
            new_argvar = ret.arg.var + "x"
            # We do this to replace var <- var + "x" in the result of the lambda, using the context
            context_save = self.walker.context
            self.walker.context = {self.arg.var : CircuitMLVar(new_argvar, self.walker)}
            res = ret.res.substitute(debug=False)
            self.walker.context = context_save
            ret.res = res
            ret.arg.var = new_argvar

        # Making a copy of the context to avoid local variables of lambda functions to get out of scope
        context_copy = copy(self.walker.context)
        used_varnames_copy = copy(self.walker.used_varnames)
        # Substitute result
        ret.res = ret.res.substitute (debug=False)
        # Restore context and used varnames (a lambda expression should not alter used_varnames (local variables))
        self.walker.context = context_copy
        self.walker.used_varnames = used_varnames_copy

        if not ret.res:
            return None

        return ret

    def get_used_varnames (self):
        return self.res.get_used_varnames() - self.arg.get_used_varnames()


class CircuitMLAssignment (CircuitMLExpr):

    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.var = CircuitMLVar(node.var, walker)
        self.val = walker.walk(node.val)
        self.next = walker.walk(node.next)

    def substitute (self, debug=True):
        value = self.val.substitute(debug=debug)
        if not value:
            return None
        self.walker.context[self.var.var] = value

        # Assignments are the only place where used variables are updated
        self.walker.used_varnames = self.walker.used_varnames.union(set(self.get_used_varnames()) - set(self.walker.used_varnames))

        return self.next.substitute(debug=debug)

    def get_used_varnames (self):
        return set(self.var.var).union(self.val.get_used_varnames())


class CircuitMLComposite (CircuitMLExpr):

    def __init__ (self, node, walker):
        CircuitMLExpr.__init__ (self, node, walker)
        self.fun = walker.walk(node.fun)
        self.arg = walker.walk(node.arg)

    def substitute (self, debug=True):
        ret = copy(self)

        ret.fun = self.fun.substitute(debug=debug)
        if not ret.fun:
            return None
        ret.arg = self.arg.substitute(debug=debug)
        if not ret.arg:
            return None

        if type(ret.fun) == CircuitMLLambda:
            # Substitute [fun] argument by [arg] in [fun] res
            self.walker.context[ret.fun.arg.var] = ret.arg
            res = ret.fun.res.substitute(debug=debug)
            # Restore context
            self.walker.context.pop(ret.fun.arg.var)
            return res

        elif type(ret.fun) == CircuitMLNot:
            if type(ret.arg) != CircuitMLBool:
                if debug:
                    print ("Applying not on non-boolean expression of type", type(ret.arg))
                    return None
                else:
                    return ret
            return ret.fun.operation (ret.arg.value)

        elif CircuitMLOperation in getancestors(type(ret.fun)):
            if type(ret.arg) != CircuitMLPair:
                if debug:
                    print ("Applying logical operator taking 2 parameters on non-pair expression of type", type(ret.arg))
                    return None
                else:
                    return ret

            if CircuitMLLogicalOperation in getancestors(type(ret.fun)):
                # Unable to substitute to value
                if type(ret.arg.left) != CircuitMLBool or type(ret.arg.right) != CircuitMLBool:
                    if debug:
                        print ("& | Nand operators need boolean parameters to produce a result, not a pair ({},{})".format(type(ret.arg.left),type(ret.arg.right)))
                        return None
                    else:
                        return ret

                return ret.fun.operation(ret.arg.left.value, ret.arg.right.value)

            # Fst or Snd
            return ret.fun.operation(ret.arg.left, ret.arg.right)

        # Function not found
        if type(ret.fun) == CircuitMLVar:
            if debug:
                print ("Function {} not found".format(ret.fun.var))
                return None
            else:
                return ret

        if debug:
            print ("Trying to apply argument to non-function expression of type", type(ret))
            return None
        else:
            return ret

    def get_used_varnames (self):
        return self.fun.get_used_varnames().union(self.arg.get_used_varnames())


class CircuitMLCondition (CircuitMLExpr):

    def __init__ (self, node, walker):
        CircuitMLExpr.__init__(self, node, walker)
        self.cond = walker.walk (node.cond)
        self.if_statement = walker.walk (node.if_statement)
        self.else_statement = walker.walk (node.else_statement)

    def substitute (self, debug=True):
        ret = copy(self)
        ret.cond = self.cond.substitute(debug=debug)
        if type(ret.cond) != CircuitMLBool:
            if debug:
                print ("If Then Else condition cannot be of type {}".format(type(ret.cond)))
                return None
            else:
                return ret

        if (ret.cond.value):
            return self.if_statement.substitute(debug=debug)
        return self.else_statement.substitute(debug=debug)

    def get_used_varnames (self):
        return self.cond.get_used_varnames().union(self.if_statement.get_used_varnames()).union(self.else_statement.get_used_varnames())