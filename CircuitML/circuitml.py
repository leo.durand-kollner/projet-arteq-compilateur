import tatsu
from tatsu.ast import AST

import json

from walker import CircuitMLWalker
from expr import *

f = open('grammar.ebnf', 'r')
grammar = f.read()

# Compile grammar to get parser
parser = tatsu.compile(grammar, asmodel=True)

def test_expr (expr_str):
    print ("***** Trying expression \"\"\"{}\"\"\" *****".format(expr_str))

    # Get AST from parser (could fail if the expression does not respect the grammar)
    ast = parser.parse(expr_str)
    print ("\n\nAST : ")
    print (json.dumps(ast.asjson(), indent=4))

    print ("\n\n** Walking expression **\n")

    # Walk AST to evaluate expressions
    result = CircuitMLWalker().walk (ast)

    if result:
        print("\n+ Syntax is ok")
        print("\n** Evaluation **\n")

        evaluation = result.substitute()

        if evaluation:
            print ("+ Success :")
            print (evaluation)
            if type(evaluation) == CircuitMLBool:
                print(evaluation.value)
            elif type(evaluation) == CircuitMLVar:
                print(evaluation.var)

        else:
            print ("Evaluation error")

    else:
        print ("\n- Syntax error")

expressions = [
    'f x y z', # Evaluation error : f not found
    'a = True; a = False; | (a,False)', # False
    'a = x; ((a))', # x
    'x = !; a = x; a False', # True
    'y = True; y = &(y,False); y', # False
    'x = |; f = x; f (False, False)', # False
    'a = True; (Lambda x : & (a, Fst x)) (True, False)', # True
    'Snd (Fst, Lambda x : Nand(Fst x,False)) (False, True)', # True
    'f = Lambda x: (y = True; False); y', # y
    'f = (Lambda x: &(x,True)); f False', # False
    'y = x; f = Lambda x: y; x = False; f True', # False
    'y = x; f = Lambda x: &(x,y); x = False; f True', # False
    'f = Lambda x : (x,True); & f False', # Syntax error : & takes a pair as argument
    'a = False; If a Then False Else True Endif', # True (negation)
    'If a Then False Else True Endif', # Evaluation error : conditions are not values
    'f = Lambda x: If Fst x Then Snd x Else ! (Snd x) Endif; f (False, True)', # False (f is an equality comparison)
    'f = Lambda x: | (& (Fst x, Snd x), & (! (Fst x), ! (Snd x))); f (False, True)' # False (another way to do it)
]

for expr in expressions:
    print("\n")
    test_expr (expr)