import tatsu

from abc import ABC, abstractmethod
from inspect import getmro as getancestors
from copy import copy

class MiniMLExpr (ABC):
    """
    Class to represent a general MiniML expression
    """
    def __init__ (self, node, walker):
        self.node = node
        self.walker = walker

    def substitute (self, debug=True):
        """
        Substitute all variables by available expressions (in [context])
        Should return an expression without modifying substitute's called expression
        """
        return self

    def get_used_varnames (self):
        """
        Get used variables (different from walker context since some variables can be undefined)
        """
        return set()


class MiniMLInt (MiniMLExpr):
    def __init__ (self, node, walker):
        MiniMLExpr.__init__(self, node, walker)
        self.value = int(node)


class MiniMLVar (MiniMLExpr):
    def __init__ (self, node, walker):
        MiniMLExpr.__init__(self, node, walker)
        self.var = node

    def substitute (self, debug=True):
        if self.var in self.walker.context:
            return self.walker.context[self.var].substitute(debug=debug)
        return self

    def get_used_varnames (self):
        return set(self.var)


class MiniMLPair (MiniMLExpr):
    def __init__ (self, node, walker):
        MiniMLExpr.__init__(self, node, walker)
        self.left = walker.walk(node.left)
        self.right = walker.walk(node.right)

    def substitute (self, debug=True):
        left = self.left.substitute(debug=debug)
        if not left:
            return None

        right = self.right.substitute(debug=debug)
        if not right:
            return None

        ret = copy(self)
        ret.left = left
        ret.right = right
        return ret

    def get_used_varnames (self):
        return self.left.get_used_varnames().union(self.right.get_used_varnames())


# A general operation on two arguments : f : (x,y)
class MiniMLOperation (MiniMLExpr, ABC):
    @abstractmethod
    def operation (self, x, y):
        pass

    def get_used_varnames (self):
        return set()

# +, -, / or *
class MiniMLArithmeticsOperation (MiniMLOperation, ABC):
    pass

class MiniMLAddition (MiniMLArithmeticsOperation):
    def operation (self, x, y):
        return MiniMLInt(x+y, self.walker)

class MiniMLSubtraction (MiniMLArithmeticsOperation):
    def operation (self, x, y):
        return MiniMLInt(x-y, self.walker)

class MiniMLMultiplication (MiniMLArithmeticsOperation):
    def operation (self, x, y):
        return MiniMLInt(x*y, self.walker)

class MiniMLDivision (MiniMLArithmeticsOperation):
    def operation (self, x, y):
        try:
            res = x//y
            return MiniMLInt(res, self.walker)
        except ZeroDivisionError:
            print ("Division by zero !")
            return None

class MiniMLFirst (MiniMLOperation):
    def operation (self, x,y):
        return x

class MiniMLSecond (MiniMLOperation):
    def operation (self, x,y):
        return y


class MiniMLLambda (MiniMLExpr):

    def __init__ (self, node, walker):
        MiniMLExpr.__init__(self, node, walker)
        self.arg = MiniMLVar(node.arg, walker)
        self.res = walker.walk(node.res)

    def substitute (self, debug=True):
        # First check if argument variable name is already used
        # Example of where this is needed : """y = x; f = Lambda x : +(x,y); x = 0; f 1"""
        # This should return 1, not 2
        ret = copy(self)
        while ret.arg.var in self.walker.used_varnames:
            # Rename arg variable to [varname] + "x"
            new_argvar = ret.arg.var + "x"
            # We do this to replace var <- var + "x" in the result of the lambda, using the context
            context_save = self.walker.context
            self.walker.context = {self.arg.var : MiniMLVar(new_argvar, self.walker)}
            res = ret.res.substitute(debug=False)
            self.walker.context = context_save
            ret.res = res
            ret.arg.var = new_argvar

        # Making a copy of the context to avoid local variables of lambda functions to get out of scope
        context_copy = copy(self.walker.context)
        used_varnames_copy = copy(self.walker.used_varnames)
        # Substitute result
        ret.res = ret.res.substitute (debug=False)
        # Restore context and used varnames (a lambda expression should not alter used_varnames (local variables))
        self.walker.context = context_copy
        self.walker.used_varnames = used_varnames_copy

        if not ret.res:
            return None

        return ret

    def get_used_varnames (self):
        return self.res.get_used_varnames() - self.arg.get_used_varnames()


class MiniMLAssignment (MiniMLExpr):

    def __init__ (self, node, walker):
        MiniMLExpr.__init__(self, node, walker)
        self.var = MiniMLVar(node.var, walker)
        self.val = walker.walk(node.val)
        self.next = walker.walk(node.next)

    def substitute (self, debug=True):
        value = self.val.substitute(debug=debug)
        if not value:
            return None
        self.walker.context[self.var.var] = value

        # Assignments are the only place where used variables are updated
        self.walker.used_varnames = self.walker.used_varnames.union(set(self.get_used_varnames()) - set(self.walker.used_varnames))

        return self.next.substitute(debug=debug)

    def get_used_varnames (self):
        return set(self.var.var).union(self.val.get_used_varnames())


class MiniMLComposite (MiniMLExpr):

    def __init__ (self, node, walker):
        MiniMLExpr.__init__ (self, node, walker)
        self.fun = walker.walk(node.fun)
        self.arg = walker.walk(node.arg)

    def substitute (self, debug=True):
        ret = copy(self)

        ret.fun = self.fun.substitute(debug=debug)
        if not ret.fun:
            return None
        ret.arg = self.arg.substitute(debug=debug)
        if not ret.arg:
            return None


        if type(ret.fun) == MiniMLLambda:
            # Substitute [fun] argument by [arg] in [fun] res
            self.walker.context[ret.fun.arg.var] = ret.arg
            res = ret.fun.res.substitute(debug=debug)
            # Restore context
            self.walker.context.pop(ret.fun.arg.var)
            return res


        elif MiniMLOperation in getancestors(type(ret.fun)):
            if type(ret.arg) != MiniMLPair:
                if debug:
                    print ("Applying + on non-pair expression of type", type(ret.arg))
                    return None
                else:
                    return ret

            if MiniMLArithmeticsOperation in getancestors(type(ret.fun)):
                # Unable to substitute to value
                if type(ret.arg.left) != MiniMLInt or type(ret.arg.right) != MiniMLInt:
                    if debug:
                        print ("+ - * / operators need integers parameters to produce a result, not a pair ({},{})".format(type(ret.arg.left),type(ret.arg.right)))
                        return None
                    else:
                        return ret

                return ret.fun.operation(ret.arg.left.value, ret.arg.right.value)

            # Fst or Snd
            return ret.fun.operation(ret.arg.left, ret.arg.right)

        # Function not found
        if type(ret.fun) == MiniMLVar:
            if debug:
                print ("Function {} not found".format(ret.fun.var))
                return None
            else:
                return ret

        if debug:
            print ("Trying to apply argument to non-function expression of type", type(ret))
            return None
        else:
            return ret

    def get_used_varnames (self):
        return self.fun.get_used_varnames().union(self.arg.get_used_varnames())