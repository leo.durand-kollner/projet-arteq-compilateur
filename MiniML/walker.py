from tatsu.walkers import NodeWalker # Explore AST

from expr import *

# Not working because of lambdas evaluation
class MiniMLWalker (NodeWalker):
    def __init__(self):
        NodeWalker.__init__(self)
        self.context = {} # Variables values
        self.used_varnames = set() # Used variable names

    def walk_object (self, node):
        print("Error : found node of type", type(node))
        return node

    def walk_int (self, node):
        print("Int", node)
        return MiniMLInt (node, self)

    def walk_str (self, node):
        print("Var", node)
        return MiniMLVar (node, self)

    def walk__addition(self, node):
        print("Add")
        return MiniMLAddition (node, self)

    def walk__subtraction(self, node):
        print("Subtract")
        return MiniMLSubtraction (node, self)

    def walk__multiplication(self, node):
        print("Multiply")
        return MiniMLMultiplication (node, self)

    def walk__division(self, node):
        print("Divide")
        return MiniMLDivision (node, self)

    def walk__first(self, node):
        print("First")
        return MiniMLFirst (node, self)

    def walk__second(self, node):
        print("Second")
        return MiniMLSecond (node, self)

    def walk__pair(self, node):
        print("Pair")
        return MiniMLPair (node, self)

    def walk__lambda(self, node):
        print("Lambda")
        return MiniMLLambda (node, self)

    def walk__assignment(self, node):
        print("Assignment")
        return MiniMLAssignment (node, self)

    def walk__composite(self, node):
        print("Composite")
        return MiniMLComposite (node, self)