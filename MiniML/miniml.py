import tatsu
from tatsu.ast import AST

import json

from walker import MiniMLWalker
from expr import *

f = open('grammar.ebnf', 'r')
grammar = f.read()

# Compile grammar to get parser
parser = tatsu.compile(grammar, asmodel=True)

def test_expr (expr_str):
    print ("***** Trying expression \"\"\"{}\"\"\" *****".format(expr_str))

    # Get AST from parser (could fail if the expression does not respect the grammar)
    ast = parser.parse(expr_str)
    print ("\n\nAST : ")
    print (json.dumps(ast.asjson(), indent=4))

    print ("\n\n** Walking expression **\n")

    # Walk AST to evaluate expressions
    result = MiniMLWalker().walk (ast)

    if result:
        print("\n+ Syntax is ok")
        print("\n** Evaluation **\n")

        evaluation = result.substitute()

        if evaluation:
            print ("+ Success :")
            print (evaluation)
            if type(evaluation) == MiniMLInt:
                print(evaluation.value)
            elif type(evaluation) == MiniMLVar:
                print(evaluation.var)

        else:
            print ("Evaluation error")

    else:
        print ("\n- Syntax error")

expressions = [
    'f x y z', # Evaluation error : f not found
    'a = 1; a = 2; +(a,1)', # 3
    'a = x; ((a))', # x
    'x = +; a = x; a 2', # Evaluation error : + should apply to a pair
    'y = 6; y = /(y,2); y', # 3
    'x = *; f = x; f (2,3)', # 6
    'a = -1; (Lambda x : * (a, Fst x)) (2, 3)', # -2
    'Snd (Fst, Lambda x : +(Fst x,1)) (0,1)', # 1
    'f = Lambda x: (y = 1; 2); y', # y
    'f = (Lambda x: +(x,1)); f 2', # 3
    'y = x; f = Lambda x: y; x = 0; f 1', # 0, not 1
    'y = x; f = Lambda x: +(x,y); x = 5; f 2', # 7, not 4
    'f = Lambda x : (x,1); + f 1' # Evaluation error : + should apply to a pair
]

for expr in expressions:
    print("\n")
    test_expr (expr)